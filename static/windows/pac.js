function FindProxyForURL(url, host) {
	if (
		host == 'localhost' ||
		host == '127.0.0.1' ||
		host == 'ss.maowtm.org' ||
		/^127\.\d+\.\d+\.\d+$/.test(host)
	) {
		return 'DIRECT';
	}
	return 'SOCKS5 127.0.0.1:1080';
}
