FROM node:lts
WORKDIR /usr/src/app
COPY . .
RUN npm ci && npm run build
EXPOSE 3000
ENV NODE_ENV=production
ENV HOST=0.0.0.0
ENV PORT=3000
ENV ORIGIN=https://ss.maowtm.org
CMD ["node", "index.js"]
