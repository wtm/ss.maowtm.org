import { COOKIE_NAME, checkPassword } from '$lib/auth';
import { fail, redirect } from '@sveltejs/kit';
import type { Actions, PageServerLoad } from './$types';

export const load: PageServerLoad = async ({ locals }) => {
	if (locals.sessionValidated) {
		throw redirect(302, '/info');
	}
	return {};
};

export const actions: Actions = {
	async login({ request, cookies }) {
		const data = await request.formData();
		const password = data.get('password');
		if (typeof password !== 'string' || !checkPassword(password)) {
			return fail(403, { error: 'Password incorrect' });
		}

		cookies.set(COOKIE_NAME, password, { sameSite: 'lax' });

		throw redirect(302, '/info');
	},

	async logout({ cookies }) {
		cookies.delete(COOKIE_NAME);
		throw redirect(302, '/');
	}
};
