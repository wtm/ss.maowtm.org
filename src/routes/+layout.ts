import type { LayoutLoad } from './$types';

export const load: LayoutLoad = async ({ data }) => {
	return {
		title: 'ss.mw',
		sessionValidated: data.sessionValidated
	};
};
