import { redirect } from '@sveltejs/kit';
import type { LayoutServerLoad } from '../$types';
import { getHostTrojanConfig, generateV2RayNgTrojanURL } from '$lib/trojan-config';

export const load: LayoutServerLoad = async ({ locals }) => {
	if (!locals.sessionValidated) {
		throw redirect(302, '/');
	}
	let trojanConfig = await getHostTrojanConfig();
	return {
		trojanConfig,
		v2rayTrojanUrl: generateV2RayNgTrojanURL(trojanConfig)
	};
};
