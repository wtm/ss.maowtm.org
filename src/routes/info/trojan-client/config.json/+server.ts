import { error } from '@sveltejs/kit';
import type { RequestEvent } from './$types';
import { getHostTrojanConfig, generateTrojanClientConfigJSON } from '$lib/trojan-config';

export async function GET({ locals }: RequestEvent): Promise<Response> {
	if (!locals.sessionValidated) {
		throw error(403, 'Forbidden');
	}
	let conf = await getHostTrojanConfig();
	let json = generateTrojanClientConfigJSON(conf);
	return new Response(json, {
		headers: {
			'content-type': 'application/json',
			'content-disposition': 'attachment; filename="config.json"'
		}
	});
}
