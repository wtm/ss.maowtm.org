import dotenv from 'dotenv';
dotenv.config();

import type { Handle } from '@sveltejs/kit';

import { COOKIE_NAME, checkPassword } from '$lib/auth';

export const handle: Handle = async ({ event, resolve }) => {
	let cookie = event.cookies.get(COOKIE_NAME);
	if (typeof cookie != 'string' || !checkPassword(cookie)) {
		event.locals.sessionValidated = false;
	} else {
		event.locals.sessionValidated = true;
	}
	let res = await resolve(event);
	res.headers.set('Strict-Transport-Security', 'max-age=63072000; includeSubDomains; preload');
	return res;
};
