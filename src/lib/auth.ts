export const COOKIE_NAME = 'portal-password';

export function checkPassword(password: string) {
	const PORTAL_PASSWORD = process.env.PORTAL_PASSWORD;
	if (!PORTAL_PASSWORD) {
		return false;
	}
	return password === PORTAL_PASSWORD;
}
