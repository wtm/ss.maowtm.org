import { readFile } from 'node:fs/promises';

interface ProxyServerConfig {
	server: string;
	password: string;
}

export async function getHostTrojanConfig(): Promise<ProxyServerConfig> {
	const TROJAN_CONFIG_PATH = process.env.TROJAN_CONFIG_PATH ?? '/etc/trojan/config.json';
	let json_content = await readFile(TROJAN_CONFIG_PATH, { encoding: 'utf-8' });
	let json = JSON.parse(json_content);

	return {
		server: 'ss.maowtm.org',
		password: json.password[0] as string
	};
}

export function generateTrojanClientConfigJSON({ server, password }: ProxyServerConfig) {
	let obj = {
		run_type: 'client',
		local_addr: '127.0.0.1',
		local_port: 1080,
		remote_addr: server,
		remote_port: 443,
		password: [password],
		log_level: 1,
		ssl: {
			verify: true,
			verify_hostname: true,
			cert: '',
			cipher:
				'ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES128-SHA:ECDHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA:AES128-SHA:AES256-SHA:DES-CBC3-SHA',
			cipher_tls13: 'TLS_AES_128_GCM_SHA256:TLS_CHACHA20_POLY1305_SHA256:TLS_AES_256_GCM_SHA384',
			sni: '',
			alpn: ['h2', 'http/1.1'],
			reuse_session: true,
			session_ticket: false,
			curves: ''
		},
		tcp: {
			no_delay: true,
			keep_alive: true,
			reuse_port: false,
			fast_open: false,
			fast_open_qlen: 20
		}
	};
	return JSON.stringify(obj, null, 2);
}

export function generateV2RayNgTrojanURL({ server, password }: ProxyServerConfig) {
	return `trojan://${encodeURIComponent(
		password
	)}@${server}:443?security=tls&alpn=h2,http/1.1&headerType=none&type=tcp#ss.maowtm.org`;
}
