// See https://kit.svelte.dev/docs/types#app
// for information about these interfaces
declare global {
	namespace App {
		// interface Error {}
		interface Locals {
			sessionValidated: boolean;
		}
		interface PageData {
			sessionValidated: boolean;
		}
		// interface Platform {}
	}
}

export {};
